const admin = require("firebase-admin");
var serviceAccount = require("../auth_token.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://twitch-interractive.firebaseio.com",
});
const db = admin.firestore();
const auth = admin.auth();

module.exports = {
  db,
  admin,
  auth,
};
