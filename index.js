require("dotenv").config();
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const routes = require("./routes/routes");
const app = express();
const expressWs = require("express-ws")(app);
const { db } = require("./database/index");

app.enable("trust proxy");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

routes.crud(app);

exports.api = (req, res) => {
  console.log("I am a log entry!");
  console.error("I am an error!");
  res.end();
};

app.listen(3600, () => {
  console.log("⚡⚡⚡ Server starting on port 3600");
});
