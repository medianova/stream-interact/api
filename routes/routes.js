const obs = require("../src/Controllers/External/Obs.controller");
const project = require("../src/Controllers/Project.controller");
const { postResult, sendQuestion } = require("../src/Controllers/External/Twitch.controller");
const {
  register,
  identify,
  unregister,
  broadcastAll,
} = require("../src/Services/WebSocket.services");
const user = require("../src/Controllers/User.controller");
const auth = require("../src/Controllers/Auth.controller");
const reward = require("../src/Controllers/Reward.controller");
const {checkToken} = require("../src/middlewares/middleware.functions")

module.exports = {
  crud: (app) => {
    app
      .get("/", (req, res) => {
        res.status(200).send({ version: 2 });
      })
      .ws("/obs", (ws) => {
        console.log("🤝🤝🤝 Connection established");
        register(ws);

        ws.on("close", () => {
          let id = identify(ws);
          unregister(id);
          console.log("👋👋👋 Connection stopped for : ", id);
        });

        ws.on("message", (payload) => {
          let id = identify(ws);
          console.log("📨📨📨 Message receive from : ", id);
          let { method, ...data } = JSON.parse(payload);
          eval(`obs.${method}(id, data)`);
        });
      })
      // AUTH
      .post("/register", (req, res) => {
        auth.register(req, res);
      })
      .post("/login", (req, res) => {
        auth.login(req, res);
      })
      // REWARD
      .post("/rewards", (req, res) => {
        reward.triggerReward(req, res);
      })
      // PROJECT
      .get("/project", checkToken ,(req, res) => {
        project.index(req, res);
      })
      .post("/project", checkToken ,(req, res) => {
        project.create(req, res);
      })
      .post("/question/lunch",(req, res) => {
        sendQuestion(req, res);
      })
      // TWITCH
      .post("/twitch/postResult", (req, res) => {
        postResult(req, res);
      })
      .post("/broadcast", (req,res) => {
        broadcastAll(req.body);
        res.json("send");
      });
  },
};
