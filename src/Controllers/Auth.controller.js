const auth = require("../Services/Auth.services")

async function register(req, res) 
{
    // TODO : Validate data

    const response = await auth.register(req.body);
    let code = (response.message === "success") ? 200 : 400;

    return res.status(code).json(response);
}

async function login(req, res)
{
    // TODO : Validate data
    
    const response = await auth.login(req.body);
    let code = (response.message === "success") ? 200 : 400;

    return res.status(code).json(response);
}

module.exports = {
    register,
    login
};
