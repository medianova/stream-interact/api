const obs = require("../../Services/External/Obs.services");

module.exports = {
  start: (uuid, { projectId, privateKey }) => {
    obs.start(uuid, projectId, privateKey);
  },
  // getNextScenes: (req, res, db, errorMessage) => {
  //   obs
  //     .getNextScenes(req, res, db, errorMessage)
  //     .then((result) => {
  //       return res.status(200).json(result);
  //     })
  //     .catch((error) => {
  //       return res.send(error);
  //     });
  // },
  // pushQuestion: (req, res, db, errorMessage) => {
  //   obs
  //     .pushQuestion(req, res, db, errorMessage)
  //     .then((result) => {
  //       return res.status(200).json(result);
  //     })
  //     .catch((error) => {
  //       return res.send(error);
  //     });
  // },
};
