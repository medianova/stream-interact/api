const { postResult } = require("../../Services/External/Twitch.services");
const twitch = require("../../Services/External/Twitch.services");
const { default: axios } = require("axios");

module.exports = {
  postResult: (req, res) => {
    let result = postResult(req.body);

    if (result) {
      res.status(200);
    } else {
      res.status(400);
    }
  },
  sendQuestion: (req, res) => {
    let question = req.body.question;
    question.projectId = req.body.projectId
    console.log("Fireee !")
    axios.post('https://extension-backend.stream-interact.medianova.xyz/question', {question: question})
      .catch(e => console.log(e))
      .then(function (repsonse) {
        res.status(200).json("Bravo")
      })
  }
};
