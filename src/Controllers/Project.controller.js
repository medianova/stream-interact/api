const projectServices = require("../Services/Project.services");


// INDEX
const index = async (req, res) => {
  let result = await projectServices.getAllProjectFromUser(req.user);

  const code = result !== null ? 200 : 204;

  let final = [];

  for (const [key, value] of Object.entries(result)) {
    final.push({
      id: key,
      ...value,
      createdAt: value.createdAt.toDate()
    });
  }

  return res.status(code).json(final);
}

// CREATE
const create = async (req, res) => {
  const result = await projectServices.createProject(req.user, req.body.project);

  const code = result !== null ? 200 : 204;
  const response = result !== null ? {"message": "success" }: null;

  return res.status(code).json(response);
}

module.exports = {
  index,
  create
};
