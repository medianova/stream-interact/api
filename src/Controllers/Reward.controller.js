const rewardService = require("../Services/Reward.services")

const triggerReward = async (req, res) => {
    const {channelId, triggerName, rewardId} = req.body;

    if (!channelId) {
        res.status(401).json({
            message: "errors",
            errors: [
                "ChannelId missing"
            ]
        })
    }

    if (!triggerName) {
        res.status(401).json({
            message: "errors",
            errors: [
                "TriggerName missing"
            ]
        })
    }

    if (!rewardId) {
        res.status(401).json({
            message: "errors",
            errors: [
                "RewardId missing"
            ]
        })
    }
    
    await rewardService.trigger({channelId, triggerName, rewardId});
    return res.status(200).json({
        message: "success"
    })
}

module.export = {
    triggerReward
}