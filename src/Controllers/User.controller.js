const {create} = require('../Services/User.services');


module.exports = {
    create: (req, res) => {
        // TODO : Validate info
        const {email, id} = req.body
        const response = create(email, id);
        return response.message === 'success' ? res.status(200).json(response.data)  : res.status(400).json(response.data);
    }
};
