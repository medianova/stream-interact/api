const { v4: uuidv4 } = require("uuid");
const { db, admin } = require("../../database");
const logger = require("../Services/Logger.services.js");

module.exports = {
  // Get all projects
  getAllProjects: async () => {
    const data = db.collection("projects");
    let response = [];
    await data
      .get()
      .then((querySnapshot) => {
        let docs = querySnapshot.docs;
        for (let doc of docs) {
          response.push(doc.data());
        }
      })
      .catch((error) => {
        return null;
      });
    return response;
  },
  // Get project by Id
  getProjectById: async (id) => {
    const query = db.collection("projects").doc(id);
    let response = (await query.get()).data();
    return response ? response : null;
  },
  // Get project by ChannelId
  getProjectByChannelId: async (channelId) => {
    const query = db.collection("projects").where("channelId", "==", channelId);;
    let response = (await query.get()).data();
    return response ? response : null;
  },
  // Get project by UserId
  getProjectByUserId: async (userId) => {
    const query = db.collection("projects").where("authorId", "==", userId);
    let response = [];
    await query.get().then((results) => {
      results.forEach((doc) => {
        response[doc.id] = doc.data();
      });
    });
    return response ? response : null;
  },
  // Create a project
  createProject: async (project) => {
    let data = {
      ...project,
      createdAt: new Date(),
    };
    return await db
      .collection("projects")
      .doc(uuidv4())
      .set(data)
      .then((result) => {
        logger.info("Create project", { project });
        return true;
      })
      .catch((errors) => {
        logger.error("Create project", errors);
        return null;
      });
  },
  // Update a project
  updateProject: async (id, project) => {
    return await db
      .collection("projects")
      .doc(id)
      .set({
        ...project,
        ...{ updatedAt: new Date() },
      })
      .then((result) => {
        logger.info("Update project", { projectId: id });
        return result;
      })
      .catch((error) => {
        logger.error("Update project", { error });
        return null;
      });
  },
};
