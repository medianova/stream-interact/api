const { db, admin } = require("../../database");

async function getUserbyId(id) {
  const document = db.collection("users").doc(id);
  let response = (await document.get()).data();

  return response ? response : null;
}

async function getUserbyEmail(email) {
    const document = db.collection("users").where("email", "==", email);
    let response = null;
    await document.get()
        .then(snap => {
            snap.forEach(el => {
                response = el.data()
            })
        });
    return response ? response : null;
}

async function create(id, name, email, password) {
    return await db.collection('users').doc(id)
            .set({
                id: id,
                name: name,
                email: email,
                password: password,
                createdAt: new Date(),
                updateAt: new Date(),
                lastSignInTime: new Date()
            })
            .catch(error => {
                return {
                    message: 'error',
                    data: error
                };
            })
            .then(() => {
                return {
                    message: 'success',
                    data: {
                        user : {
                            id: id,
                            name: name,
                            email: email
                        }
                    }
                };
            })
}

module.exports = {
  getUserbyId,
  getUserbyEmail,
  create
};
