module.exports = {
  /**
   *
   * @param {object} data
   * @returns {object}
   */
  showScene: (data) => {
    return {
      action: "showScene",
      data: {
        sceneName: data.sceneName,
      },
    };
  },
};
