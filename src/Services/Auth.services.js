const { getUserbyEmail } = require("../Repository/User.repository");
const userService = require("../Services/User.services")
const crypto = require("crypto");
const { v4: uuidv4 } = require("uuid");
const jwt = require("jsonwebtoken");
const SECRET = process.env.JWT_TOKEN

const getHashedPassword = (password) => {
    const sha256 = crypto.createHash('sha256');
    const hash = sha256.update(password).digest('base64');
    return hash;
}

async function register({name, email, password, confirmed_password})
{
    if (password !== confirmed_password) {
        return {
            message: 'error',
            errors: [
                'Le mot de passe et le mot de passe confirmé sont différents'
            ]
        }
    }

    
    if (await getUserbyEmail(email) !== null) {
        return {
            message: 'error',
            errors: [
                "L'adresse mail est déjà utilisé"
            ]
        }
    }

    const passwordHashed = getHashedPassword(password);
    const id = uuidv4();
    const user = await userService.create(id, name, email, passwordHashed);

    if(!user) {
        return null;
    }

    return await connect(user);
}

async function login({email, password})
{
    const user = await getUserbyEmail(email);

    if (!user) {
        return {
            'message': 'error',
            'errors': [
                'Utilisateur non trouvé'
            ]
        }
    }

    const passwordHashed = getHashedPassword(password);

    if (user.password !== passwordHashed)
    {
        return {
            'message': 'error',
            'errors': [
                "Le mot de passe n'est pas correct"
            ]
        }
    }

    return await connect(user);
}

async function connect(user) 
{
    const token = jwt.sign({
        id: user.id,
        email: user.email
    },SECRET ,{ expiresIn: '3 hours'});

    return {
        message: 'success',
        access_token: token
    };
}




module.exports = {
    register,
    login
};
