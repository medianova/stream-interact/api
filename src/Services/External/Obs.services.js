const { startQuestion } = require("./Twitch.services");
const { getProjectById } = require("../../Repository/Project.repository");
const { showScene } = require("../Action.services");
const { info, error } = require("../Logger.services");
const { getUserbyId } = require("../../Repository/User.repository");
const { broadcast } = require("../WebSocket.services");

module.exports = {
  /**
   * Initiate connexion with OBS by delivering the project
   * @param {string} uuid
   * @param {string} projectId
   * @param {string} privateKey
   * @returns
   */
  start: async (uuid, projectId, privateKey) => {
    // Validation
    if (!projectId || !privateKey) {
      const response = {
        errors: {},
      };
      if (!projectId) {
        response.errors.projectId = "projectId missing";
      }
      if (!privateKey) {
        response.errors.privateKey = "privateKey missing";
      }
      return broadcast(uuid, response);
    }

    // Recupérer le project
    const project = await getProjectById(projectId);

    if (!project) {
      return broadcast(uuid, {
        errors: { projectId: "this projectId doesnt belong to a project" },
      });
    }

    // Recupérer l'utilisateur
    let author = await getUserbyId(project.authorId);

    if (!author) {
      return broadcast(uuid, {
        errors: { author: "the authorId linked to this project is not found" },
      });
    }

    // Vérification de la clé de l'utilisateur
    if (author.id !== privateKey) {
      return broadcast(uuid, {
        errors: {
          privateKey:
            "the privateKey don't correspond to the author of the project",
        },
      });
    }

    // On retourne le projet
    return broadcast(uuid, { project: project });
  },

  /**
   * Prepare the payload for OBS
   * @param {string} uuid
   * @param {string} actionName
   * @param {Object} data
   * @returns {boolean}
   */
  prepareAction: async (actionName, data) => {
    let action = {
      showScene: showScene(data),
    };
    const payload = await action[actionName];
    if (!payload) {
      return false;
    }
    return payload;
  },

  /**
   * Send a action on Obs
   * @param {string} uuid
   * @param {Object} payload
   * @returns {boolean}
   */
  sendAction: async (uuid, payload) => {
    wss.broadcast;
    await broadcast(uuid, payload);
    return true;
  },

  /**
   * Launch a question on Twitch
   * @param {string} uuid
   * @param {object} data
   * @returns
   */
  startQuestion: async (uuid, data) => {
    // TODO : Validation des données

    // Recupérer le project
    const project = getProjectById(data.projectId);

    if (!project) {
      error("startQuestion", {
        projectId: "Not defined",
        questionId: "Not defined",
      });
      return broadcast(uuid, {
        errors: { projectId: "this projectId doesnt belong to a project" },
      });
    }

    const question = project.questions.find((el) => {
      return el.id === data.questionId;
    });

    if (!question) {
      error("startQuestion", {
        projectId: project.id,
        questionId: "Not defined",
      });
      return broadcast(uuid, {
        errors: { question: "this questionId doesnt belong to this project" },
      });
    }

    let status = await startQuestion(project, question);
    status
      ? info("startQuestion", {
          projectId: project.id,
          questionId: data.questionId,
        })
      : error("startQuestion", {
          projectId: project.id,
          questionId: data.questionId,
        });
  },
};
