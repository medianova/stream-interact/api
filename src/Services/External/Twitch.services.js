const {
  getProjectById,
  updateProject,
} = require("../../Repository/Project.repository");
const { error } = require("../Logger.services");
const { handleResults } = require("../Results.services");
const axios = require("axios");
const { broadcastAll } = require("../WebSocket.services");
const { prepareAction } = require("./Obs.services");

module.exports = {
  /**
   * Send question to Twitch API
   * @param {object} project
   * @param {object} question
   */
  startQuestion: async (project, question) => {
    const data = {
      question: {
        projectId: project.id,
        questionId: question.id,
        ...question,
      },
    };
    // TODO : Send to TWITCH API
  },

  postResult: async (data) => {
    const { projectId, questionId, results } = data;

    if (!projectId || !questionId || !results) {
      // TODO : Logger les données manquantes
    }

    const project = await getProjectById(projectId);

    if (!project) {
      error("postResult", {
        project: "project not found",
      });
      return false;
    }

    const questionIndex = project.questions.findIndex((el) => {
      return el.id === data.questionId;
    });

    const question = project.questions[questionIndex];

    if (!question) {
      error("postResult", {
        projectId: project.id,
        questionId: "Not found in project",
      });
      return false;
    }

    /* const {finalResults, bestResultId} = handleResults(results);
    project.questions[questionIndex].results = finalResults;

    const triggerId = question[bestResultId].triggerId;

    const triggerIndex = project.triggers.findIndex((el) => {
      return el.id === triggerId;
    });

    const trigger = project.trigger[triggerIndex];

    const action = prepareAction(trigger.actionName, trigger.data)

    await updateProject(projectId, project);

    broadcastAll(action) */

    broadcastAll({
      action: "showScene",
      data: {
        sceneName: "Scene 1",
      },
    });

    return true;
  },
};
