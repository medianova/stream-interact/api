const axios = require('axios')
module.exports = {
    getToken: (req, res) => {
        const code = req.body.code
        const request = {
            method: 'post',
            url: 'https://id.twitch.tv/oauth2/token?client_id=wwwivubx3eou87gg5vhzzsspws1wsj&client_secret=pkjgz5lwnd36njjskyguxm5emi9p06&code='+ code +'&grant_type=authorization_code&redirect_uri=http://localhost:8080/twitchconnect',
        }
        axios(request).then(r => {
            if (r.data && r.data.access_token) {
                // Twitch renvoie le token de l'utilisateur. Quand on query avec ça renvoie l'user
                const request = {
                    method: 'get',
                    url: 'https://api.twitch.tv/helix/users',
                    headers: {
                        'Client-ID': 'wwwivubx3eou87gg5vhzzsspws1wsj',
                        'Authorization': 'Bearer ' + r.data.access_token
                    }
                }
                axios(request).then(r => {
                    if (r.data && r.data.data && r.data.data[0]) {
                        return r.data.data[0];
                    } else {
                        res.status(400).send('No user data')
                    }
                }).catch(e => {
                    res.status(400).send('No user data')
                })
            } else {
                res.status(500).send('No access token')
            }
        }).catch(e => {
            res.status(500).send('Invalid code')
        })

    }
}
