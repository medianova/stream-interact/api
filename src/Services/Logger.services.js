const { db } = require("../../database");
const { v4: uuidv4 } = require("uuid");

module.exports = {
  /**
   * Log an debug
   * @param {string} method
   * @param {object} data
   */
  debug: (method, data) => {
    db.collection("log").doc(uuidv4()).set({
      type: "debug",
      timestamp: Date.now(),
      method: method,
      data: data,
    });
  },

  /**
   * Log an info
   * @param {string} method
   * @param {object} data
   */
  info: (method, data) => {
    db.collection("log").doc(uuidv4()).set({
      type: "info",
      timestamp: Date.now(),
      method: method,
      data: data,
    });
  },

  /**
   * Log an error
   * @param {string} method
   * @param {object} data
   */
  error: (method, data) => {
    db.collection("log").doc(uuidv4()).set({
      type: "error",
      timestamp: Date.now(),
      method: method,
      data: data,
    });
  },
};
