const repository = require("../Repository/Project.repository");

// Get all project from a user
const getAllProjectFromUser = async (user) => {
  if (!user.id) {
    return null;
  }

  const result = await repository.getProjectByUserId(user.id);
  return !result ? null : result;
}

// Create a project
const createProject = async ({email, id}, project) => {

  const finalProject = {
    authorId: id,
    authorEmail: email,
    ...project
  }

  let result = await repository.createProject(finalProject);

  return !result ? null : result;
}

module.exports = {
  // Update a project
  updateProject: async (req, res) => {
    let result = repository.updateProject(req.body.project.id, req.body.project);

    if (!result) {
      return res.status(400).send();
    }

    return res.status(201).json({
      message: "The project has been successfully updated !",
    });
  },
  getAllProjectFromUser,
  createProject
};
