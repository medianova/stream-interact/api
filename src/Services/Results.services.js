function sortCountVote(a, b) {
  if (a.votes > b.votes) {
    return -1;
  }
  if (a.votes < b.votes) {
    return 1;
  }
  return 0;
}

module.exports = {
  handleResults: (results) => {
    const finalResults = [];

    let bestResultId = null;

    results.sort(sortCountVote);

    results.forEach((result, index) => {
      if (index !== 0) {
        if (
          result.votes === results[index - 1].votes &&
          finalResults[index - 1].winner
        ) {
          highestValue = true;
          bestResultId = result.id
        } else {
          highestValue = false;
        }
      } else {
        highestValue = true;
      }
      finalResults.push({
        answerId: result.id,
        count: result.votes,
        winner: highestValue,
      });
    });

    return {finalResults, bestResultId};
  },
};
