const { getProjectByChannelId } = require("../Repository/Project.repository");
const { prepareAction } = require("./External/Obs.services");
const { getConnection } = require("./WebSocket.services");


const trigger = async ({channelId, triggerName, rewardId}) => {
    const project = getProjectByChannelId(channelId);
    const trigger = project.triggers.find((el) => {
        return el.id === triggerName;
    });

    const conn = getConnection()[0];
    prepareAction(conn.id, trigger.actions[0].method, trigger.actions[0].data);
}


module.export = {
    trigger
}