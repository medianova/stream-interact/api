const { db, admin } = require("../../database");
const userRepo = require("../Repository/User.repository");

function generateKey(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

module.exports = {
    // Create
    create: async (id, name, email, password) => {
        const response = await userRepo.create(id, name, email, password);

        if (response.messsage === 'error'){
            return null
        }

        return response.data;
    }
};
