const { v4: uuidv4 } = require("uuid");

const connections = [];
const ids = [];

function register(ws) {
  connections.push(ws);
  let id = uuidv4();
  ids.push({
    id: id,
    index: connections.length - 1,
  });
  console.log("Id attributed : ", id);
  ws.send("Connection established");
}

function identify(ws) {
  let indexOfConn = connections.indexOf(ws);
  let result = ids.find((el) => {
    return el.index === indexOfConn;
  });
  return result.id;
}

function unregister(id) {
  let resultIndex = ids.findIndex((el) => {
    return el.id === id;
  });
  connections.splice(ids[resultIndex].index, 1);
  ids.splice(resultIndex, 1);
}

function broadcast(id, data) {
  let result = ids.find((el) => {
    return el.id === id;
  });
  connections[result.index].send(JSON.stringify(data));
}

function broadcastAll(data) {
  const e = JSON.stringify(data)
  connections.forEach((con) => {
    con.send(e);
  });
}

function getConnection() {
  return ids;
}

module.exports = { register, identify, unregister, broadcast, broadcastAll, getConnection };
