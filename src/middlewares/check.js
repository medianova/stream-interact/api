module.exports = {
    privateKey: (req, res, db, errorMessage) => next => {

        if(!req.body.privateKey){
            return res.status(400).json({ "code": 400, "message": "Bad request", "reason": "privateKey is required" });
        }

        const document = db.collection('users').doc(req.params.userId);
        let response = (document.get()).data();

        if(!response){
            return res.status(404).send(errorMessage(404, "User not found"));
        }

        if(!req.body.privateKey === response.privateKey){
            return res.status(401).send(errorMessage(401, "Unauthorized", 'privateKey is invalid'));
        }

        next();
    }
}
