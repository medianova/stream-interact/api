const jwt = require("jsonwebtoken")
const SECRET = process.env.JWT_TOKEN

async function extractToken(headers) {
  if (typeof headers !== 'string') {
    return null
  }
  const matches = headers.match(/(bearer)\s+(\S+)/i);
  return matches && matches[2]
}


module.exports = {
  isFromDashboard: async (req, res, next) => {
    let authToken = req.headers["autorization"];
    if (typeof authToken !== "undefined") {
      const bearer = authToken.split(" ");
      const token = bearer[1];
      if (process.env.API_TOKEN === token) {
        res.status(403);
      }
    } else {
      res.status(403);
    }
    next();
  },
  checkToken: async (req, res, next) => {
    const token = req.headers.authorization && await extractToken(req.headers.authorization);

    if (!token) {
      return res.status(401).json({
        message: 'error',
        errors: [
          'Token missing'
        ]
      })
    }

    jwt.verify(token, SECRET, (error, decodedToken) => {
      if (error) {
        res.status(401).json({
          message: 'error',
          errors: [
            'Bad token',
            error
          ]
        })
      } else {
        const {email, id} = decodedToken;
        req.user = {email, id};
        return next()
      }
    })
  }
};
